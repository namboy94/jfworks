# Contributing to Jfworks

If you would like to contribute to the development of Jfworks, please follow the
following procedure:

First, fork the repository over at http://gitlab.nambisun.net

Then, write your changes and put in a merge request.